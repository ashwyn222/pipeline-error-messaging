import { getLocaleDateFormat } from '@angular/common';
import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  data: any;
  fileContent: any = "";

  constructor(private appService: AppService) {
    this.data = {Contents:[]};
    this.getData();
  }

  getData() {
    this.appService.getS3Logs().subscribe(
      (data) => {
        console.log(data);
        this.data = data;
      }, (err) => {
        
      }
    )
  }

  viewLog(key: string) {
    this.appService.viewLog(key).subscribe(
      (data) => {
        this.fileContent = data;
      }, (err) => {
        console.log(err);
      }
    )
  }

  downloadLog() {
    this.appService.downloadLog().subscribe(
      (data) => {
        console.log(data);
        this.data = data;
      }, (err) => {
        
      }
    )
  }

  deleteLog(key: string) {
    this.appService.deleteLog(key).subscribe(
      (data) => {
        console.log(`${key} is deleted successfully`);
        // remove from data
        this.data.Contents = this.data.Contents.filter((element:any) => element.Key !== key);
      }, (err) => {
        console.error(err);
      }
    )
  }
  


}
