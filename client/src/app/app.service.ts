import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AppService {
  private base_url:string = 'http://localhost:5000/api/log/';
  constructor(private http: HttpClient) { }

  getS3Logs(): Observable<any[]> {
    return this.http.get<any>(`${this.base_url}getS3Logs`);
  }

  viewLog(logFile:string) {
    let file = encodeURIComponent(logFile);
    return this.http.get(`${this.base_url}viewLog?key=${file}`, {responseType: 'text'});
  }

  downloadLog(): Observable<any[]> {
    return this.http.get<any>(`${this.base_url}downloadLog`);
  }

  deleteLog(logFile:string): Observable<any[]> {
    let file = encodeURIComponent(logFile);
    return this.http.get<any>(`${this.base_url}deleteLog?key=${file}`);
  }

}
