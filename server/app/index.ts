import http from "http";
import app from "./src/app";
import logger from "./src/config/logger";


/**
 * Setting up the HTTPS Server to serve the ExpressJS Application
 */

let appServer = http.createServer(app);


appServer.listen(5000);
appServer.on("error", onError);
appServer.on("listening", onListening);

function onListening() {
    const addr = appServer.address();
    const bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr.port;

    console.log(`Securely listening on ${bind}`);
}

function onError(error) {
    if (error.syscall !== "listen") {
        throw error;
    }
}
