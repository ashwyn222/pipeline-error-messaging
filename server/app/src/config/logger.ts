import * as winston from "winston";
import {format, transports} from "winston";
import environment from "../util/getEnvironment";

const myFormat = winston.format.printf((info) => {
    return `${info.timestamp} ${info.level} : ${info.message}`;
});
const infoLogs = environment.getValueOf("LOG_FILE_PATH") + environment.getValueOf("INFO_LOG_FILE_NAME");
const errorLogs = environment.getValueOf("LOG_FILE_PATH") + environment.getValueOf("ERROR_LOG_FILE_NAME");

const infoLogger = winston.createLogger({
    level: "info",
    format: format.combine(
        winston.format.timestamp(),
        myFormat
    ),
    transports: [
        new transports.File({filename: infoLogs, level: "info"}),
    ],
});


const errorLogger = winston.createLogger({
    level: "error",
    format: format.combine(
        winston.format.timestamp(),
        myFormat,
    ),
    transports: [
        new transports.File({filename: errorLogs, level: "error"}),
        new transports.Console({level: "error"})
    ],
});


const logger = {infoLogger, errorLogger};
export default logger;
