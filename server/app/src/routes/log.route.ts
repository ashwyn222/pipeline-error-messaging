import * as express from "express";
var fs = require('fs');
import AWS from 'aws-sdk';

AWS.config.update({accessKeyId: 'AKIAULWYBREMSNUR3VWK', secretAccessKey: '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', region: 'us-west-2'});
var s3 = new AWS.S3();

var allErrors = {
    "fatals" : ["DOTA001F", "DOTA002F", "DOTA003F", "DOTA004F", "DOTJ002F", "DOTJ003F", "DOTJ004F", "DOTJ005F", "DOTJ006F", "DOTJ001F", "DOTJ012F", "DOTJ015F", "DOTJ016F", "DOTJ017F", "DOTJ022F", "DOTJ034F", "DOTJ035F", "DOTA066F", "PDFX004F", "PDFX005F"],

    "errors" : ["DOTA007E", "DOTA008E", "DOTA009E", "DOTA010E", "DOTJ007E", "DOTJ008E", "DOTJ009E", "DOTJ010E", "DOTJ011E", "DOTJ013E", "DOTJ019E", "DOTJ021E", "DOTJ023E", "DOTJ025E", "DOTJ026E", "DOTJ028E", "DOTJ032E", "DOTJ033E", "DOTJ038E", "DOTJ039E", "DOTJ040E", "DOTJ041E", "DOTJ042E", "DOTJ046E", "DOTJ051E", "DOTJ052E", "DOTX005E", "DOTX006E", "DOTX008E", "DOTX010E", "DOTX013E", "DOTX014E", "DOTX015E", "DOTX017E", "DOTX020E", "DOTX021E", "DOTX024E", "DOTX025E", "DOTX028E", "DOTX031E", "DOTX032E", "DOTX033E", "DOTX034E", "DOTX035E", "DOTX036E", "DOTX044E", "DOTX053E", "PDFJ001E", "PDFJ002E", "PDFX006E", "PDFX011E", "PDFX009E", "FODC0002"],

    "warnings" : ["DOTA005W", "DOTA006W", "DOTJ014W", "DOTJ020W", "DOTJ021W", "DOTJ024W", "DOTJ027W", "DOTJ036W", "DOTJ037W", "DOTJ043W", "DOTJ044W", "DOTJ045W", "DOTJ049W", "DOTJ050W", "DOTJ053W", "DOTX001W", "DOTX002W", "DOTX008W", "DOTX009W", "DOTX011W", "DOTX012W", "DOTX016W", "DOTX019W", "DOTX022W", "DOTX023W", "DOTX026W", "DOTX027W", "DOTX030W", "DOTX037W", "DOTX039W", "DOTX041W", "DOTX045W", "DOTX046W", "DOTX047W", "DOTX050W", "DOTX051W", "DOTX052W", "DOTX054W", "DOTX055W", "DOTX056W", "DOTX057W", "DOTX058W", "DOTX060W", "DOTX061W", "DOTX063W", "DOTX064W", "DOTX065W", "DOTA067W", "DOTA068W", "PDFX001W", "PDFX002W", "PDFX003W", "PDFX007W", "PDFX008W", "PDFX010W"],

    "information" : ["DOTJ018I", "DOTJ029I", "DOTJ030I", "DOTJ031I", "DOTJ045I", "DOTJ047I", "DOTJ048I", "DOTX003I", "DOTX004I", "DOTX007I", "DOTX018I", "DOTX029I", "DOTX038I", "DOTX040I", "DOTX042I", "DOTX043I", "DOTX048I", "DOTX049I", "DOTX059I", "DOTX062I"]
}

const LogRoutes = express.Router();

LogRoutes.get("/", (req, res, next) => {
    console.log("Inside log route");
    res
        .status(200)
        .send("Invoked log get request");
});

LogRoutes.get("/getS3Logs", (req, res, next) => {
    console.log("inside get S3 logs");
    var params = { 
        Bucket: 'ux-content-dev',
        Delimiter: '/',
        Prefix: 'firo-transform-logs/'
    }
   
    s3.listObjects(params, function (err, data) {
        if(err)throw err;
        console.log(data);
        res.status(200).send(data);
    });
});

LogRoutes.get("/viewLog", (req, res, next) => {
    var params = { 
        Bucket: 'ux-content-dev',
        Key: `${req.query.key}`
    }
    console.log(params);

    s3.getObject(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        } else {
            console.log("getObject Successful");
            res.setHeader(
                "Content-disposition",
                `attachment; filename=${req.query.key}`
            )
            res.setHeader("Content-Type", data.ContentType);
            res.status(200).send(data.Body);
            res.end();
        }
    });
    
});

LogRoutes.get("/deleteLog", (req, res, next) => {
    var params = { 
        Bucket: 'ux-content-dev',
        Key: `${req.query.key}`
    }

    s3.deleteObject(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        } else {
            console.log("Successfully deleted");
            res.status(200).send(data);
        }
    });
    
});

LogRoutes.get("/stats", (req, res, next) => {
    let {type, key} = req.query;

    let params = { 
        Bucket: 'ux-content-dev',
        Key: `${key}`
    }

    s3.getObject(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        } else {
            let logStr = data.Body.toString('utf-8')
        
            let stats;
            if(type === 'html') {
                stats = `<ul>
                        <li>Fatals: ${getStats('fatals', logStr)}</li>
                        <li>Errors: ${getStats('errors', logStr)}</li>
                        <li>Warnings: ${getStats('warnings', logStr)}</li>
                        <li>Information: ${getStats('information', logStr)}</li>
                    </ul>`
            } else {
                stats = {
                    "fatals": getStats('fatals', logStr),
                    "errors": getStats('errors', logStr),
                    "warnings": getStats('warnings', logStr),
                    "information": getStats('information', logStr)
                };
            }
        
            res.status(200).send(stats);
        }
    });
});

LogRoutes.get("/summary", (req, res, next) => {
    let {type, key} = req.query;

    let params = { 
        Bucket: 'ux-content-dev',
        Key: `${key}`
    }

    s3.getObject(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        } else {
            let logStr = data.Body.toString('utf-8')
            let summary;

            if(type === 'html') {
                summary = `<h3>Fatals:</h3><ol>${getSummaryinHtml('fatals', logStr)}</ol>
                            <h3>Errors:</h3><ol>${getSummaryinHtml('errors', logStr)}</ol>
                            <h3>Warnings:</h3><ol>${getSummaryinHtml('warnings', logStr)}</ol>
                            <h3>Information:</h3><ol>${getSummaryinHtml('information', logStr)}</ol>`;

            } else {
                summary = {
                    "fatals": getSummaryinJson('fatals', logStr),
                    "errors": getSummaryinJson('errors', logStr),
                    "warnings": getSummaryinJson('warnings', logStr),
                    "information": getSummaryinJson('information', logStr)
                };
            }
            
            res.status(200).send(summary);
        }
    });
});

var getStats = (type, logStr) => {
    let count = 0;
    
    allErrors[type].forEach(elem => {
        count += logStr.split(elem).length - 1;
    });

    return count;
}


var getSummaryinHtml = (type, logStr) => {
    var summary = "";
    allErrors[type].forEach((searchKeyword) => {
        var indexOccurence = logStr.indexOf(searchKeyword, 0);

        while(indexOccurence >= 0) {
            summary += `<li>${logStr.substring(indexOccurence-1, logStr.indexOf('\n', indexOccurence + 1))}</li>`;
            indexOccurence = logStr.indexOf(searchKeyword, indexOccurence + 1);
        }
    });

    if(summary.length === 0) {
        summary = `<div>None</div>`;
    }

    return summary;
}

var getSummaryinJson = (type, logStr) => {
    var summary = [];
    allErrors[type].forEach((searchKeyword) => {
        var indexOccurence = logStr.indexOf(searchKeyword, 0);

        while(indexOccurence >= 0) {
            summary.push(`${logStr.substring(indexOccurence-1, logStr.indexOf('\n', indexOccurence + 1))}`);
            indexOccurence = logStr.indexOf(searchKeyword, indexOccurence + 1);
        }
    });

    return summary;
}


export default LogRoutes;
