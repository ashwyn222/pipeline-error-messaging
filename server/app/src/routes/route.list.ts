import * as express from "express";
import LogRoutes from "./log.route";

const appRoutes = express.Router();


appRoutes.use("/log", LogRoutes);




export default appRoutes;
