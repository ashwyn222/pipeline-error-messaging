export class SDPError extends Error {
    private errorType: string;
    private customMessage: string;
    constructor(type: string, message: string, customMessage: string) {
        super();
        this.errorType = type;
        this.message = message;
        this.customMessage = customMessage;
    }
}
