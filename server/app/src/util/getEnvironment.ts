import dotenv from "dotenv";
const appConfig = dotenv.config();

const configValues = Object.values(appConfig)[0];
const environment = {
    getValueOf: (property: string) => {
        return configValues[property];
    }
}
export default environment;
