import {Request, Response} from "express";
import * as jwt from "jsonwebtoken";

export const ValidateRequests = (req, res, next) => {
    try {
        /**
         * Check if Authorization header is present
         */
        const token = req.header("Authorization");
        if (!token) {
            console.log('error', `Request from IP ${req.ip} unauthorized`);
            return res
                .status(401)
                .send("Cookie Token Not present.");
        }
        const bearerToken = token.split(" ");
        jwt.verify(bearerToken[1], process.env.AUTH_SECRET_KEY, (err, decoded) => {
            if (err) {
                console.log('error', "Token expired");
                return res
                    .status(403)
                    .send("Token expired.");
            }
            req.userInfo = decoded;
        })
        next();
    } catch (e) {
        console.error(e);
        next(e);
    }
};
