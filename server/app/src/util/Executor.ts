import {SDPError} from "./SDPError";

export class Executor {
    public static execute = async (sequelizeMethod: any) => {
        try {
            const result = await sequelizeMethod();
            return Promise.resolve ({
                            data: result,
                            isError: false,
                        });
        } catch (e) {
            throw new SDPError("DB_ERROR", e.message, "Error performing operation.");
        }
        /*return sequlizeMethod()
            .then((success) => {
                return {
                    data: success,
                    isError: false,
                };
            })
            .catch((errorObject) => {
                throw new SDPError("DB_ERROR", errorObject.message, errorObject.message);
                /!*return Promise.reject({
                    error: errorObject,
                    isError: true,
                });*!/
            });*/
    }
}

export default Executor;
