import {NextFunction, Request, Response} from "express";

const ErrorHandler = (err, req: Request, res: Response, next: NextFunction) => {

    next();
};
export default ErrorHandler;
