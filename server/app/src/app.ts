import bodyparser from "body-parser";
import compression from "compression";
import cors from "cors";
import express from "express";
import helmet from "helmet";
import appRoutes from "./routes/route.list";
import ErrorHandler from "./util/ErrorHandler";
import {ValidateRequests} from "./util/ValidateRequests";

/**
 * Setting up the Express Application
 */

const app = express();
app.set("strict routing", true);
app.use(compression());
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());
/*
express.json({
    type: "application/json",
})
*/
app.use(helmet());
app.use(cors());
// app.use(ValidateRequests);
app.use("/api", appRoutes);
app.use(ErrorHandler);
export default app;
