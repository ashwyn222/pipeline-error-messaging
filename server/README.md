## Development server

Run `npm run start-dev` for a dev server. The server will be opened on `http://localhost:5000/`. The server will automatically reload if you change any of the source files.